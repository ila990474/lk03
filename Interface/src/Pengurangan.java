class Pengurangan implements Kalkulator {
    //Do your magic here...
    @Override //menandakan bahwa method ini merupakan override atau penggantian dari method yang ada di interface
    public double hitung(double operan1, double operan2){
        return operan1 - operan2; //mengembalikan nilai hasil pengurangan dari operan1 dan operan2
    }

}