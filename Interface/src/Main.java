/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..." 
 * interface akan lebih cocok digunakan daripada abstract class jika kalkulator yang akan dibuat punya beberapa 
 * jenis operasi matematika yang tidak saling berhubungan/ tidak punya kesamaan. Misal membuat interface kalkulator 
 * yang berisi satu method hitung. Kemudian membuat class-class yang mengikuti interface Kalkulator dan menentukan 
 * cara menghitung sesuai dengan operasi matematika yang diinginkan, seperti tambah, kurang, kali, bagi, pangkat, akar, 
 * logaritma, trigonometri, dan lain-lain. Sehingga dapat membuat kalkulator yang lebih bebas dan mudah diubah-ubah tanpa 
 * harus ikut aturan atau bentuk dari abstract kelas.
 * Sedangkan abstract class tidak cocok digunakan dalam kondisi ini karena abstract class mengharuskan adanya hubungan atau kesamaan 
 * antara subclass-subclassnya. Jika menggunakan abstract class Kalkulator, maka harus menentukan hal-hal yang harus dimiliki 
 * oleh semua class yang dibuat dari abstract class Kalkulator. Sehingga menyulitkan jika membuat kalkulator yang bisa melakukan operasi
 * matematika yang berbeda-beda
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}