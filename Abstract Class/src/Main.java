/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Jawab disini..." 
 * abstract class  akan lebih cocok digunakan daripada interface jika akan membuat kalkulator yang memiliki beberapa
 * fungsi yang sama untuk semua jenis operasi matematika. Misalkan seperti soal ini kita bisa membuat abstract class
 * dengan operan1 dan operan2, method setOperan untuk mengatur nilai operan1 dan operan2, dan method abstrak hitung 
 * untuk menghitung hasil operasi. Kemudian, membuat class-class yang dibuat dari abstract class Kalkulator dan 
 * menentukan cara menghitung sesuai dengan jenis operasi yang diinginkan, seperti tambah, kurang, kali, atau bagi. 
 * Sehingga menghindari menulis ulang hal yang sama.
 * sedangkan jika menggunakan interface kalkulator pada keadaan ini, maka harus mendefinisikan ulang atribut operan1 
 * dan operan2 dan method setOperan di setiap class yang mengimplementasikan interface tersebut. Sehingga akan banyak
 * tulisan yang sama.
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}