abstract class Kalkulator {
    double operan1, operan2;
    //Do your magic here...
    public abstract void setOperan(double operand1, double operand2); //method abstrak yang berfungsi untuk mengatur nilai dari operan1 dan operan2
    public abstract double hitung(); //method perhitungan dari inputan pengguna
    
}