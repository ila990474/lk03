class Pertambahan extends Kalkulator {
    double operan1, operan2;
    //Do your magic here...
    @Override //method menggantikan atau menimpa metode yang sama dari kelas induk
    public void setOperan(double operand1, double operand2){ //mengatur nilai dari bidang operan1 dan operan2
        this.operan1 = operand1; 
        this.operan2 = operand2;
    }

    @Override 
    public double hitung(){
        return operan1+operan2; //mengembalikan hasil dari penjumlahan antara bidang operan1 dan operan2.
    }
}